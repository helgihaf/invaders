﻿using Invaders.Engine;

namespace Invaders
{
    internal class PlayerInput : IPlayerInput
    {
        public PlayerInput()
        {
        }

        public Direction Direction { get; set; }

        public bool Fire { get; set; }
    }
}