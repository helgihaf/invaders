﻿using Invaders.Engine;
using System;
using System.Threading;

namespace Invaders
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to start game");
            Console.ReadKey();

            var canvas = new BufferCanvas(Console.WindowWidth, Console.WindowHeight - 1);
            var random = new Random();
            var playerInputProvider = new PlayerInputProvider();
            var game = new Game(canvas, random, playerInputProvider);

            game.Initialize();
            game.Run();

            Console.ReadKey();
        }

        private static void GameCore(Game game, PlayerInput playerInput)
        {
            game.Draw();
            Thread.Sleep(30);
            game.Pulse();

        }
    }
}
