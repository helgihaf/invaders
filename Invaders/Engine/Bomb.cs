﻿namespace Invaders.Engine
{
    public class Bomb : BaseObject
    {
        public Bomb()
        {
            Sprite = new Sprite(1, 1, "*", CanvasColor.White);
        }

        public override void Draw(ICanvas canvas)
        {
            Sprite.Draw(canvas, X, Y);
        }
    }
}
