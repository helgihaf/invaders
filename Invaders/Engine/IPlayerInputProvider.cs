﻿namespace Invaders.Engine
{
    public interface IPlayerInputProvider
    {
        IPlayerInput Get();
    }
}
