﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public class WorstInvader : Invader
    {
        private const string shape =
            @"/\" +
            @"^^";

        public WorstInvader() : base(30)
        {
            NormalSprite = new Sprite(2, 2, shape, CanvasColor.White);
            Sprite = NormalSprite;
        }
    }
}
