﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Invaders.Engine
{
    public abstract class BaseObject
    {
        public Sprite Sprite { get; protected set; }
        public double X { get; set; }
        public double Y { get; set; }

        protected bool IsHitBy(BaseObject other, out Point point)
        {
            point = new Point();

            if (!CollidesWith(this, other))
            {
                return false;
            }

            // Now check "pixel by pixel" for collision, using other as a reference
            for (int x = 0; x < other.Sprite.Width; x++)
            {
                for (int y = 0; y < other.Sprite.Height; y++)
                {
                    int ownX = Convert.ToInt32(other.X + x - X);
                    int ownY = Convert.ToInt32(other.Y + y - Y);
                    char c = Sprite.GetPixelAt(ownX, ownY);
                    if (c != ' ')
                    {
                        point = new Point(ownX, ownY);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool CollidesWith(BaseObject external)
        {
            return CollidesWith(this, external);
        }

        public static bool CollidesWith(BaseObject a, BaseObject b)
        {
            int ax1 = Convert.ToInt32(a.X);
            int ay1 = Convert.ToInt32(a.Y);
            int ax2 = Convert.ToInt32(a.X + a.Sprite.Width - 1);
            int ay2 = Convert.ToInt32(a.Y + a.Sprite.Height - 1);

            int bx1 = Convert.ToInt32(b.X);
            int by1 = Convert.ToInt32(b.Y);
            int bx2 = Convert.ToInt32(b.X + b.Sprite.Width - 1);
            int by2 = Convert.ToInt32(b.Y + b.Sprite.Height - 1);

            return
                ax1 <= bx2 && ax2 >= bx1 &&
                ay1 <= by2 && ay2 >= by1;
        }

        public abstract void Draw(ICanvas canvas);
    }
}
