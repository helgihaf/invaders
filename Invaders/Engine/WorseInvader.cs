﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public class WorseInvader : Invader
    {
        private const string shape =
            @"[]" +
            @"TT";

        public WorseInvader() : base(20)
        {
            NormalSprite = new Sprite(2, 2, shape, CanvasColor.White);
            Sprite = NormalSprite;
        }
    }
}
