﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public class BadInvader : Invader
    {
        private const string shape =
            @"()" +
            @"/\";

        public BadInvader() : base(10)
        {
            NormalSprite = new Sprite(2, 2, shape, CanvasColor.White);

            Sprite = NormalSprite;
        }
    }
}
