﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public class Shot : BaseObject
    {
        public Shot()
        {
            Sprite = new Sprite(1, 1, "|", CanvasColor.White);
        }

        public override void Draw(ICanvas canvas)
        {
            Sprite.Draw(canvas, X, Y);
        }
    }
}
