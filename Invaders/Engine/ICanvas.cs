﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public interface ICanvas
    {
        int Width { get; }
        int Height { get; }

        void Put(double x, double y, string s, CanvasColor canvasColor);
        void Clear();

        void Apply();
    }
}
