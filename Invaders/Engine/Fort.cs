﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Invaders.Engine
{
    public class Fort : BaseObject
    {
        const string image =
            " ######## " +
            "##########" +
            "###    ###" + 
            "###    ###";

        public Fort()
        {
            Sprite = new Sprite(10, 4, image, CanvasColor.Green);
        }

        public override void Draw(ICanvas canvas)
        {
            Sprite.Draw(canvas, X, Y);
        }

        public bool CheckAndHandleHit(BaseObject baseObject)
        {
            if (IsHitBy(baseObject, out Point point))
            {
                Sprite.SetPixelAt(point.X, point.Y, ' ');
                return true;
            }

            return false;
        }
    }
}
