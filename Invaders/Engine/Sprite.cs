﻿using System;
using System.Drawing;

namespace Invaders.Engine
{
    public class Sprite
    {
        private readonly string image;
        private char[] currentImage;

        public Sprite(int width, int height, string image, CanvasColor color)
        {
            this.image = image ?? throw new ArgumentNullException(nameof(image));
            Color = color;
            Width = width;
            Height = height;
            currentImage = image.ToCharArray();
        }

        public int Width { get; }
        public int Height { get; }
        public CanvasColor Color { get; }

        public void Draw(ICanvas canvas, double x, double y)
        {
            for (int i = 0; i < Height; i++)
            {
                string s = new string(currentImage, i * Width, Width);
                canvas.Put(x, y + i, s, Color);
            }
        }

        public char GetPixelAt(int x, int y)
        {
            if (x < Width && y < Height)
            {
                return currentImage[y * Width + x];
            }
            else
            {
                return ' ';
            }
        }

        public void SetPixelAt(int x, int y, char c)
        {
            if (x < Width && y < Height)
            {
                currentImage[y * Width + x] = c;
            }
        }
    }
}
