﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public enum CanvasColor
    {
        Black,
        Blue, 
        Red,
        Magenta,
        Green,
        Cyan,
        Yellow,
        White
    }
}
