﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public class Exploder
    {
        public int initialCounter;
        public int currentCounter;

        public Exploder(int initialCounter)
        {
            this.initialCounter = initialCounter;
        }

        public bool IsActive { get; private set; }

        public void Explode()
        {
            IsActive = true;
            currentCounter = initialCounter;
        }

        public bool Pulse()
        {
            currentCounter--;
            if (currentCounter <= 0)
            {
                IsActive = false;
            }

            return IsActive;
        }
    }
}
