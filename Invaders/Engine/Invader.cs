﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public abstract class Invader : BaseObject
    {
        private const string explosionShape =
            @"**" +
            @"**";

        private readonly Sprite explosionSprite;

        public Invader(int points)
        {
            explosionSprite = new Sprite(2, 2, explosionShape, CanvasColor.Red);
            Points = points;
        }

        public Exploder Exploder { get; } = new Exploder(3);
        public int Points { get; }

        protected Sprite NormalSprite { get; set; }

        public override void Draw(ICanvas canvas)
        {
            if (!Exploder.IsActive)
            {
                Sprite = NormalSprite;
            }
            else
            {
                Sprite = explosionSprite;
            }

            Sprite.Draw(canvas, X, Y);
        }
    }
}
