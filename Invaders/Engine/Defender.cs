﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders.Engine
{
    public class Defender : BaseObject
    {
        private Sprite normalSprite;
        private Sprite explodingSprite;

        public Defender()
        {
            normalSprite = new Sprite(5, 1, "AAAAA", CanvasColor.Green);
            explodingSprite = new Sprite(5, 1, "*****", CanvasColor.Red);

            Sprite = normalSprite;
        }

        public int Lives { get; set; } = 3;

        public Exploder Exploder { get; } = new Exploder(60);

        public override void Draw(ICanvas canvas)
        {
            if (!Exploder.IsActive)
            {
                Sprite = normalSprite;
            }
            else
            { 
                Sprite = explodingSprite;
            }

            Sprite.Draw(canvas, X, Y);
        }
    }
}
