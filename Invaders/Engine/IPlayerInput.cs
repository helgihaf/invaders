﻿namespace Invaders.Engine
{
    public interface IPlayerInput
    {
        Direction Direction { get; }
        bool Fire { get; }
    }

    public enum Direction
    {
        None,
        Left,
        Right
    }
}