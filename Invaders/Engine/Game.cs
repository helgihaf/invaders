﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;

namespace Invaders.Engine
{
    public class Game
    {
        public const int LeftRightInvaderThreshold = 5;

        private readonly ICanvas canvas;
        private readonly Random random;
        private readonly IPlayerInputProvider playerInputProvider;
        private IPlayerInput playerInput;
        private Defender defender;
        private IList<Invader> invaders;
        private IList<Bomb> bombs;
        private Shot shot;
        private IList<Fort> forts;

        private const int invadersStartY = 2;
        private double invadersXDelta = 0.04;

        private int score;

        public Game(ICanvas canvas, Random random, IPlayerInputProvider playerInputProvider)
        {
            this.canvas = canvas ?? throw new ArgumentNullException(nameof(canvas));
            this.random = random;
            this.playerInputProvider = playerInputProvider ?? throw new ArgumentNullException(nameof(playerInputProvider));
        }

        public Defender Defender => defender;

        public IList<Invader> Invaders => invaders;

        public void Initialize()
        {
            score = 0;

            defender = new Defender
            {
                Lives = 3
            };
            defender.X = (canvas.Width - defender.Sprite.Width) / 2;
            defender.Y = canvas.Height - defender.Sprite.Height;

            forts = new List<Fort>();
            for (int i = 0; i < 4; i++)
            {
                var fort = new Fort();
                fort.X = 10 + i * fort.Sprite.Width * 2;
                fort.Y = defender.Y - fort.Sprite.Height - 2;
                forts.Add(fort);
            }

            playerInput = playerInputProvider.Get();
        }

        private void InitializeNewAttack()
        {
            invaders = new List<Invader>();
            for (int row = 0; row < 5; row++)
            {
                for (int col = 0; col < 11; col++)
                {
                    Invader invader;
                    if (row == 0)
                    {
                        invader = new WorstInvader();
                    }
                    else if (row >= 1 && row <= 2)
                    {
                        invader = new WorseInvader();
                    }
                    else
                    {
                        invader = new BadInvader();
                    }

                    invader.X = 10 + (col * (invader.Sprite.Width + 2));
                    invader.Y = invadersStartY + (row * (invader.Sprite.Height + 1));
                    invaders.Add(invader);
                }
            }

            bombs = new List<Bomb>();
        }

        public void Run()
        {
            while (Defender.Lives > 0)
            {
                InitializeNewAttack();
                if (defender.Lives < 3)
                {
                    defender.Lives++;
                }

                if (score > 0)
                {
                    score += 1000;
                }

                while (Defender.Lives > 0 && Invaders.Count > 0)
                {
                    RunCore();
                }
            }
            Draw();

            string msg = "Game Over";
            canvas.Put((canvas.Width - msg.Length) / 2, canvas.Height / 2, msg, CanvasColor.Yellow);
            canvas.Apply();
            Pause(2000);
        }

        private void Pause(int v)
        {
            Thread.Sleep(v);
        }

        private void RunCore()
        {
            Draw();
            Pause(30);
            Pulse();

            playerInput = playerInputProvider.Get();
        }


        public void Draw()
        {
            canvas.Clear();

            foreach (var fort in forts)
            {
                fort.Draw(canvas);
            }

            defender.Draw(canvas);
            foreach (var invader in invaders)
            {
                invader.Draw(canvas);
            }

            foreach (var bomb in bombs)
            {
                bomb.Draw(canvas);
            }

            if (shot != null)
            {
                shot.Draw(canvas);
            }

            DrawStatistics(canvas);
            canvas.Apply();
        }

        private void DrawStatistics(ICanvas canvas)
        {
            canvas.Put(0, 0, "Score: " + score.ToString(), CanvasColor.White);
            canvas.Put(canvas.Width - 10, 0, "Lives: " + defender.Lives.ToString(), CanvasColor.White);
        }

        // Draw
        // Move & bounds
        // Collision
        // State changes
        public void Pulse()
        {
            if (!defender.Exploder.IsActive)
            {
                MoveObjects();
                DetectCollisions();

                for (int i = 0; i < invaders.Count; i++)
                {
                    var invader = invaders[i];
                    if (invader.Exploder.IsActive)
                    {
                        if (!invader.Exploder.Pulse())
                        {
                            invaders.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }

            if (defender.Exploder.IsActive)
            {
                defender.Exploder.Pulse();
            }
        }

        private void DetectCollisions()
        {
            DetectCollisionWithForts();
            for (int i = 0; i < bombs.Count; i++)
            {
                var bomb = bombs[i];
                if (bomb.CollidesWith(defender))
                {
                    defender.Exploder.Explode();
                    defender.Lives--;
                    shot = null;
                    bombs.Clear();
                    break;
                }
            }

            if (shot != null)
            {
                foreach (var invader in invaders)
                {
                    if (invader.CollidesWith(shot))
                    {
                        invader.Exploder.Explode();
                        shot = null;
                        score += invader.Points;
                        break;
                    }
                }
            }
        }

        private void DetectCollisionWithForts()
        {
            foreach (var fort in forts)
            {
                for (int i = 0; i < bombs.Count;)
                {
                    var bomb = bombs[i];
                    if (fort.CheckAndHandleHit(bomb))
                    {
                        bombs.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }

                if (shot != null)
                {
                    if (fort.CheckAndHandleHit(shot))
                    {
                        shot = null;
                    }
                }
            }

        }

        private void MoveObjects()
        {
            MoveDefender();
            MoveInvaders();
            MoveBombs();
            MoveShot();
        }

        private void MoveDefender()
        {
            if (defender.Exploder.IsActive)
            {
                return;
            }

            if (playerInput.Direction == Direction.Left && defender.X > 0)
            {
                defender.X--;
            }
            else if (playerInput.Direction == Direction.Right && defender.X < canvas.Width - 1)
            {
                defender.X++;
            }

            if (playerInput.Fire && shot == null)
            {
                shot = new Shot();
                shot.X = defender.X + defender.Sprite.Width / 2;
                shot.Y = defender.Y;
            }
        }

        private void MoveInvaders()
        {
            foreach (var invader in invaders)
            {
                if (invader.X <= LeftRightInvaderThreshold)
                {
                    invadersXDelta = -invadersXDelta;
                    MoveAllInvadersDown();
                    break;
                }

                if (invader.X >= canvas.Width - LeftRightInvaderThreshold)
                {
                    invadersXDelta = -invadersXDelta;
                    MoveAllInvadersDown();
                    break;
                }
            }

            foreach (var invader in invaders)
            {
                // If no invaders below be than maby drop bomb
                // This method is really inefficient
                double maxY = invaders.Where(i => i.X == invader.X).Max(i => i.Y);
                if (maxY <= invader.Y)
                {
                    if (random.NextDouble() < 0.005)
                    {
                        var bomb = new Bomb();
                        bomb.X = invader.X;
                        bomb.Y = invader.Y + invader.Sprite.Height;
                        bombs.Add(bomb);
                    }
                }
                invader.X += invadersXDelta;
            }
        }

        private void MoveAllInvadersDown()
        {
            foreach (var invader in invaders)
            {
                invader.Y++;
            }
        }

        private void MoveBombs()
        {
            for (int i = 0; i < bombs.Count;)
            {
                var bomb = bombs[i];
                if (bomb.Y < canvas.Height - 1)
                {
                    bomb.Y += 0.4;
                    i++;
                }
                else
                {
                    bombs.RemoveAt(i);
                }
            }
        }

        private void MoveShot()
        {
            if (shot != null)
            {
                if (shot.Y > 0)
                {
                    shot.Y--;
                }
                else
                {
                    shot = null;
                }
            }
        }

    }
}
