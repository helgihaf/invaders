﻿using Invaders.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders
{
    internal class BufferCanvas : ICanvas
    {
        private char[] screen;
        private ConsoleColor[] colors;
        private readonly int width;
        private readonly int height;

        public BufferCanvas(int width, int height)
        {
            this.width = width;
            this.height = height;
            int size = width * height;
            screen = new char[size];
            colors = new ConsoleColor[size];
            Console.CursorVisible = false;
        }

        public int Width => width;

        public int Height => height;

        public void Clear()
        {
            Array.Clear(screen, 0, screen.Length);
            Array.Clear(colors, 0, colors.Length);
        }

        public void Put(double x, double y, string s, CanvasColor canvasColor)
        {
            int screenX = Convert.ToInt32(x);
            int screenY = Convert.ToInt32(y);

            foreach (var c in s)
            {
                screen[screenY * width + screenX] = c;
                colors[screenY * width + screenX] = Canvas.ToConsoleColor(canvasColor);
                screenX++;
            }
        }

        public void Apply()
        {
            Console.SetCursorPosition(0, 0);
            Console.Write(screen, 0, screen.Length - 2);
        }
    }
}
