﻿using Invaders.Engine;
using System;

namespace Invaders
{
    internal class PlayerInputProvider : IPlayerInputProvider
    {
        public IPlayerInput Get()
        {
            var playerInput = new PlayerInput
            {
                Direction = Direction.None,
                Fire = false
            };

            ConsoleKeyInfo key = new ConsoleKeyInfo();
            while (Console.KeyAvailable)
            {
                key = Console.ReadKey();
            }

            if (key.Key == ConsoleKey.LeftArrow)
            {
                playerInput.Direction = Direction.Left;
            }
            else if (key.Key == ConsoleKey.RightArrow)
            {
                playerInput.Direction = Direction.Right;
            }
            else if (key.Key == ConsoleKey.Spacebar)
            {
                playerInput.Fire = true;
            }

            return playerInput;
        }
    }
}
