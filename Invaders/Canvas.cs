﻿using Invaders.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Invaders
{
    internal class Canvas : ICanvas
    {
        public int Width => Console.WindowWidth;

        public int Height => Console.WindowHeight;

        public void Clear()
        {
            Console.Clear();
            Console.CursorVisible = false;
        }

        public void Put(double x, double y, string s, CanvasColor canvasColor)
        {
            Console.ForegroundColor = ToConsoleColor(canvasColor);
            Console.SetCursorPosition(Convert.ToInt32(x), Convert.ToInt32(y));
            Console.Write(s);
        }

        public void Apply()
        {

        }

        internal static ConsoleColor ToConsoleColor(CanvasColor canvasColor)
        {
            switch (canvasColor)
            {
                case CanvasColor.Black:
                    return ConsoleColor.Black;
                case CanvasColor.Blue:
                    return ConsoleColor.Blue;
                case CanvasColor.Red:
                    return ConsoleColor.Red;
                case CanvasColor.Magenta:
                    return ConsoleColor.Magenta;
                case CanvasColor.Green:
                    return ConsoleColor.Green;
                case CanvasColor.Cyan:
                    return ConsoleColor.Cyan;
                case CanvasColor.Yellow:
                    return ConsoleColor.Yellow;
                case CanvasColor.White:
                    return ConsoleColor.White;
            }

            return ConsoleColor.White;
        }
    }
}
